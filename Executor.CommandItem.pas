unit Executor.CommandItem;

interface

uses
  System.Classes,
  System.SysUtils,
  Executor.Interfaces,
  Spring.Collections;

type

  TCommandItem = class(TInterfacedObject, ICommandItem)
  private
    fID: TGUID;
    fProcessID: System.NativeUInt;
    fStatus: TCommandStatus;
    fOutputType: TOutputType;
    fThreaded: boolean;
    fOutfile: string;
    fCommandType: TCommandType;
    fOutput: IList<string>;
    fCommand: string;
    fCmd: string;
    fParam: string;
    fSettings: ISettings;

    function GetCommand: string;
    function GetCommandType: TCommandType;
    function GetID: TGUID;
    function GetOutput: IList<String>;
    function GetOutputType: TOutputType;
    function GetStatus: TCommandStatus;
    function GetThreaded: boolean;
    procedure SetCommand(const Value: string);
    procedure SetCommandType(const Value: TCommandType);
    procedure SetOutputType(const Value: TOutputType);
    procedure SetThreaded(const Value: boolean);
    function GetCmd: string;
    function GetParam: string;
    function GetOutfile: string;
    procedure SetCmdAndParam;
    function GetProcessID: System.NativeUInt;
    procedure SetProcessID(const Value: System.NativeUInt);
    function GetSettings: ISettings;
    procedure SetSettings(const Value: ISettings);
    procedure SetOutfile(const Value: string);
  public
    constructor Create;
    property ID : TGUID read GetID;
    property ProcessID : System.NativeUInt read GetProcessID write SetProcessID;
    property CommandType : TCommandType read GetCommandType write SetCommandType;
    property Cmd: string read GetCmd;
    property Param: string read GetParam;
    property Command : string read GetCommand write SetCommand;
    property Threaded : boolean read GetThreaded write SetThreaded default true;
    property OutputType : TOutputType read GetOutputType write SetOutputType default otEvented;
    property Output : IList<String> read GetOutput;
    property Outfile: string read GetOutfile write SetOutfile;
    property Settings : ISettings read GetSettings write SetSettings;
    property Status : TCommandStatus read GetStatus;
  end;

implementation

{ TCommandItem }

constructor TCommandItem.Create;
begin
  System.SysUtils.CreateGUID(fID); // Create our unique ID; used for output and identity
  fStatus := csUnknown;
  fOutfile := fID.ToString.Replace('{','').Replace('}','') + '.out';   //fID.ToString + '.out';
  fOutput := TCollections.CreateList<string>;
  fParam := '';
  fCmd := '';
  fProcessID := 0;
end;

function TCommandItem.GetID: TGUID;
begin
  Result := fID;
end;

function TCommandItem.GetCommandType: TCommandType;
begin
  Result := fCommandType;
end;

procedure TCommandItem.SetCommandType(const Value: TCommandType);
begin
  fCommandType := Value;
  SetCmdAndParam;
end;

function TCommandItem.GetCmd: string;
begin
  Result := fCmd;
end;

function TCommandItem.GetParam: string;
begin
  Result := fParam;
end;

procedure TCommandItem.SetCmdAndParam;
begin
  if (fCommandType <> ctUnknown) and (not fCommand.IsEmpty) then
  begin
    case fCommandType of
      ctCMD:
        begin
          fCmd := 'cmd.exe';
          fParam := Format(' /C %s',[fCommand]);
        end;
      ctPowershell:
        begin
          fCmd := 'powershell.exe';
          fParam := Format(' %s',[fCommand]);
          //fParam := Format(' -EncodedCommand %s',[fCommand]);
        end;
      ctPSCore:
        begin
          fCmd := 'pwsh.exe';
          fParam := Format(' %s',[fCommand]);
          //fParam := Format(' -EncodedCommand %s',[fCommand]);
        end;
    end;
  end;
end;

function TCommandItem.GetCommand: string;
begin
  Result := fCommand;
end;

procedure TCommandItem.SetCommand(const Value: string);
begin
  fCommand := Value;
  SetCmdAndParam;
end;

function TCommandItem.GetOutfile: string;
begin
  Result := fOutfile;
end;

procedure TCommandItem.SetOutfile(const Value: string);
begin
  fOutfile := Value;
end;


function TCommandItem.GetOutput: IList<String>;
begin
  Result := fOutput;
end;

function TCommandItem.GetOutputType: TOutputType;
begin
  Result := fOutputType;
end;

procedure TCommandItem.SetOutputType(const Value: TOutputType);
begin
  fOutputType := Value;
end;

function TCommandItem.GetProcessID: System.NativeUInt;
begin
  Result := fProcessID;
end;

procedure TCommandItem.SetProcessID(const Value: System.NativeUInt);
begin
  fProcessID := Value;
end;

function TCommandItem.GetStatus: TCommandStatus;
begin
  Result := fStatus;
end;

function TCommandItem.GetThreaded: boolean;
begin
  Result := fThreaded
end;

procedure TCommandItem.SetThreaded(const Value: boolean);
begin
  fThreaded := Value;
end;

function TCommandItem.GetSettings: ISettings;
begin
  Result := fSettings;
end;

procedure TCommandItem.SetSettings(const Value: ISettings);
begin
  fSettings := Value;
end;

end.

