program Executor;

uses
  Vcl.Forms,
  Executor.Interfaces in 'Executor.Interfaces.pas',
  Executor.ExecuteCommands in 'Executor.ExecuteCommands.pas',
  Executor.Settings in 'Executor.Settings.pas',
  Executor.CommandItem in 'Executor.CommandItem.pas',
  Executor.Commands in 'Executor.Commands.pas',
  FormMain in 'FormMain.pas' {Main};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TMain, Main);
  Application.Run;
end.
