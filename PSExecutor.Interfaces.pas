unit PSExecutor.Interfaces;

interface

uses
  System.Classes,
  Spring.Collections;

type
  TCommandStatus = ( csUnknown, csStarted, csCompleted, csFailed );
  TOutputType = ( otUnknown, otOutFile, otEvented, otLines );
  TCommandType = ( ctUnknown, ctCMD, ctPowershell, ctPSCore );
  TOnFail = procedure(const index: integer; const error: string) of object;
  TOnLine = procedure(const index: integer; const line: string) of object;
  TOnCommandComplete = procedure(const index: integer; const filename: string; const outfile: string ) of object;

  ISettings = interface
    ['{8BC029F3-C568-4989-A7B5-82BAEF5BE45F}']
    procedure SetPath(const Value: string);
    function GetPath: string;
    procedure SetCurrentDirectory(const Value: string);
    function GetCurrentDirectory: string;

    { Public }
    property Path : string read GetPath write SetPath;
    property CurrentDirectory : string read GetCurrentDirectory write SetCurrentDirectory;
    {
      DefaultType

    }
  end;


  ICommandItem = interface
    ['{F8FAC482-6F7F-49D4-9779-D99B14B3005B}']
    function GetID: string;
    procedure SetCommand(const Value: string);
    function GetCommand: string;
    function GetThreaded: boolean;
    procedure SetThreaded(const Value: boolean);

    { Public }
    property ID : string read GetID;
    property CommandType : TCommandType read GetCommand write SetCommand;
    property Command : string read GetCommand write SetCommand;
    property Threaded : boolean read GetThreaded write SetThreaded;

    property OutputType : TOutputType read GetThreaded write SetThreaded;
    property Output : IList<String> read GetThreaded write SetThreaded;

    property Status : TCommandStatus read GetThreaded write SetThreaded;

    {
       ID: string
       CommandType: TCommandType
       Command: string
       Status: TCommandStatus
       Threaded: boolean
       OutputType: TOutputType
       Output: IList<String>
    }
  end;

  ICommands = interface
    ['{E926A467-0F3A-4642-85DB-A9B6D1183BFE}']
    function GetSettings: ISettings;
    procedure SetSettings(const Value: ISettings);
    function First: ICommandItem;
    function Next: ICommandItem;
    { Public }
    procedure CreateItem(const aCommand: string);
    {
    procedure AddLinesFromFile(const aIndex: integer; const aFilename: string; const aDumpFilename: string);
    procedure AddLineToItem(const aIndex: integer; const aFilename: string; const aLine: string);
    function FindItem(const aFilename: string): ICommandItem;
    function GetResults: IList<string>;
    }
    function Items: IList<ICommandItem>;
    property Settings: ISettings read GetSettings write SetSettings;
  end;


  IExecuteCommands = interface
    ['{0ED758B4-562B-4D80-A13F-446ED6E231F4}']
    function GetItemIndex: integer;
    function GetSettings: ISettings;
    procedure SetSettings(const Value: ISettings);

    function GetOnCommandComplete: TOnCommandComplete;
    procedure SetOnCommandComplete(const Value: TOnCommandComplete);
    function GetOnComplete: TNotifyEvent;
    procedure SetOnComplete(const Value: TNotifyEvent);

    procedure ProcessList;
    procedure ProcessCommand(const aCommand: string );

    { Public }
    procedure Execute;
    procedure Stop;
    function HasStopped: boolean;

    property ItemIndex: integer read GetItemIndex;
    property Settings: ISettings read GetSettings write SetSettings;

    property OnFileExecutionComplete: TOnCommandComplete read GetOnCommandComplete write SetOnCommandComplete;
    property OnComplete: TNotifyEvent read GetOnComplete write SetOnComplete;


    (*
    function GetItemIndex: integer;
    function GetSettings: ISettings;
    procedure SetSettings(const Value: ISettings);
    function GetCommand: ICommand;
    procedure SetCommand(const Value: ICommand);
    function GetOnFileExecutionComplete: TOnFileExecutionComplete;
    procedure SetOnFileExecutionComplete(const Value: TOnFileExecutionComplete);
    function GetOnComplete: TNotifyEvent;
    procedure SetOnComplete(const Value: TNotifyEvent);

    procedure ProcessList;
    procedure ProcessFile(const aFilename: string );
    procedure Complete;
    procedure FileExecutionComplete(const aIndex: integer; const aID: cardinal; const aFilename: string);
    { Public }
    procedure Execute;
    procedure Stop;
    function WasStopped: boolean;

    property ItemIndex: integer read GetItemIndex;
    property Command: ICommand read GetCommand write SetCommand;
    property Settings: ISettings read GetSettings write SetSettings;
    property OnFileExecutionComplete: TOnFileExecutionComplete read GetOnFileExecutionComplete write SetOnFileExecutionComplete;
    property OnComplete: TNotifyEvent read GetOnComplete write SetOnComplete;
    *)
  end;

implementation

end.
