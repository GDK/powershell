unit Executor.Commands;

interface

uses
  System.Classes,
  System.SysUtils,
  Executor.Interfaces,
  Executor.CommandItem,
  Spring.Collections;

type

  TCommands = class(TInterfacedObject, ICommands)
  private
    List: IList<ICommandItem>;
    Index: integer;
    function First: ICommandItem;
    function Next: ICommandItem;
  public
    constructor Create;
    procedure Clear;
    procedure AddItem(const aItem: ICommandItem);
    function FindItem(const aID: string): ICommandItem;
    function Items: IList<ICommandItem>;
  end;

implementation

{ TCommands }

constructor TCommands.Create;
begin
  inherited;
  List := TCollections.CreateList<ICommandItem>;
  Index := 0;
end;

procedure TCommands.AddItem(const aItem: ICommandItem);
begin
  if aItem <> nil then List.Add(aItem);
end;

procedure TCommands.Clear;
begin
  List.Clear;
end;

function TCommands.FindItem(const aID: string): ICommandItem;
var
  item: ICommandItem;
  found: boolean;
  idx: integer;
begin
  idx := 0;
  found := false;
  Result := nil;

  while (not found) and (idx < List.Count)  do
  begin
    item := List.Items[idx];
    found := (item.ID.ToString = aID);
    if not found then Inc(idx);
  end;

  if found then Result := item;
end;

function TCommands.First: ICommandItem;
begin
  Index := 0;
  Result := List.First;
end;

function TCommands.Next: ICommandItem;
begin
  Inc( Index );
  if Index < List.Count
    then Result := List.Items[ Index ]
    else Result := nil;
end;

function TCommands.Items: IList<ICommandItem>;
begin
  Result := List;
end;

end.

