unit Executor.ExecuteCommands;

interface

uses
  System.Classes,
  System.SysUtils,
  System.RegularExpressions,
  Winapi.ShellAPI,
  Winapi.Windows,
  Vcl.Forms,
  Executor.Interfaces,
  Executor.CommandItem,
  Executor.Commands,
  Spring.Collections;

type

  TExecuteCommands = class(TInterfacedObject, IExecuteCommands)
  private
    fCancelled: boolean;
    fItemIndex: integer;
    fCommands: ICommands;
    fCurrentItem: ICommandItem;
    fSettings: ISettings;
    fOnCommandComplete: TOnCommandComplete;
    fOnLine: TOnLine;
    fOnFail: TOnFail;
    fOnComplete: TNotifyEvent;

    function GetItemIndex: integer;
    function GetOnCommandComplete: TOnCommandComplete;
    procedure SetOnCommandComplete(const Value: TOnCommandComplete);
    function GetOnLine: TOnLine;
    procedure SetOnLine(const Value: TOnLine);
    function GetOnComplete: TNotifyEvent;
    procedure SetOnComplete(const Value: TNotifyEvent);
    function GetSettings: ISettings;
    procedure SetSettings(const Value: ISettings);

    procedure ProcessList;
    procedure ProcessCommand(const aCommand: ICommandItem);
    procedure CommandComplete(const aCommand: ICommandItem);
    procedure CommandFailed(const aCommand: ICommandItem; const error: string);
    procedure Complete;

    // Actual execution routines
    procedure ThExecuteAndWait(const item: ICommandItem; const aIndex: integer;
        const aCmd, aParams: string;
        const RunAsAdmin: Boolean; const AShow: integer);
    procedure ExecuteAndWait(const item: ICommandItem; const aCmd, aParams: string;
        const RunAsAdmin: Boolean; const aShow: integer);
    procedure ExecuteAndWaitWithOutput(const item: ICommandItem; const aCmd, aParams: string;
        const RunAsAdmin: Boolean; const aShow: integer);
    function GetItems: IList<ICommandItem>;
    function GetOnFail: TOnFail;
    procedure SetOnFail(const Value: TOnFail);

  public
    constructor Create;
    procedure Execute;
    procedure Stop;
    procedure AddItem(item: ICommandItem);

    function HasStopped: boolean;

    property Items: IList<ICommandItem> read GetItems;
    property ItemIndex: integer read GetItemIndex;
    property Settings: ISettings read GetSettings write SetSettings;
    property OnLine: TOnLine read GetOnLine write SetOnLine;
    property OnFail: TOnFail read GetOnFail write SetOnFail;
    property OnCommandComplete: TOnCommandComplete read GetOnCommandComplete write SetOnCommandComplete;
    property OnComplete: TNotifyEvent read GetOnComplete write SetOnComplete;
  end;

implementation

uses
  System.Threading;

{ TExecuteCommands }

constructor TExecuteCommands.Create;
begin
  inherited;
  fCommands := TCommands.Create;
  fItemIndex := 0;
end;


procedure TExecuteCommands.AddItem(item: ICommandItem);
begin
  fCommands.AddItem(item);
end;

procedure TExecuteCommands.Execute;
begin
  fCancelled := False;
  ProcessList;
end;

procedure TExecuteCommands.Stop;
begin
  fCancelled := True;
end;

function TExecuteCommands.HasStopped: boolean;
begin
  Result := fCancelled;
end;

procedure TExecuteCommands.ProcessList;
var
  item: ICommandItem;
begin
  while (fItemIndex < fCommands.Items.Count) and (not fCancelled) do
  begin
    fCurrentItem := fCommands.Items[fItemIndex];
    ProcessCommand(fCurrentItem);
    inc(fItemIndex);
    Application.ProcessMessages;
  end;
  Complete;
end;

procedure TExecuteCommands.ProcessCommand(const aCommand: ICommandItem);
var
  id: string;
  aCmd: string;
  aParam: string;
  aQuotedOutfile: string;
begin
  if not fCancelled then
  begin
    if aCommand <> nil then
    begin
      aCommand.ProcessID := 0;
      aCmd := aCommand.Cmd;
      aParam := aCommand.Param;
    end;
    aQuotedOutfile := Format('"%s"',[IncludeTrailingPathDelimiter(aCommand.Settings.OutputFolder)+aCommand.Outfile]);
    //aCommand.Outfile := aQuotedOutfile;

    if aCommand.OutputType = otOutFile then aParam := aParam + format(' > %s',[aQuotedOutfile]);

    if aCommand.Threaded then
    begin
      ThExecuteAndWait( aCommand,fItemIndex,aCmd,aParam,False,SW_HIDE );
    end
    else
    begin
      case aCommand.OutputType of
        otOutFile :
          begin
            ExecuteAndWait( aCommand, aCmd, aParam, False, SW_HIDE );
            CommandComplete(aCommand);
          end;
        otEvented :
          begin
            ExecuteAndWaitWithOutput( aCommand, aCmd, aParam, False, SW_HIDE );
            CommandComplete(aCommand);
          end;
        otLines   :
          begin
            ExecuteAndWaitWithOutput( aCommand, aCmd, aParam, False, SW_HIDE );
            CommandComplete(aCommand);
          end;
      end;
    end;
  end;
end;

procedure TExecuteCommands.ThExecuteAndWait(const item: ICommandItem; const aIndex: integer; const aCmd, aParams: string; const RunAsAdmin: Boolean; const AShow: integer);
begin
  TTask.Run(
    procedure
    var
      id: cardinal;
    begin
      try
        case item.OutputType of
          otOutFile : ExecuteAndWait( item, aCmd, aParams, RunAsAdmin, AShow );
          otEvented : ExecuteAndWaitWithOutput( item, aCmd, aParams, RunAsAdmin, AShow );
          otLines   : ExecuteAndWaitWithOutput( item, aCmd, aParams, RunAsAdmin, AShow );
        end;

        // I hate Sleeps but I had to add this, as I got the odd occasional
        // failure messages to do with the cmd being busy, so a sleep should
        // create a mini 'back-off'; otherwise we end up missing things, not ideal !
        Sleep(100);
      finally
        TThread.Synchronize(nil,
          procedure
          begin
            CommandComplete(item);
          end );
      end;
    end );
end;

procedure TExecuteCommands.ExecuteAndWait(const item: ICommandItem; const aCmd, aParams: string; const RunAsAdmin: Boolean; const aShow: integer);
var
  sei: TShellExecuteInfo;
begin
  item.ProcessID := 0;

  FillChar(sei, SizeOf(sei), 0);
  sei.cbSize := SizeOf(sei);
  sei.Wnd := Application.Handle;
  sei.fMask := SEE_MASK_FLAG_NO_UI or SEE_MASK_NOCLOSEPROCESS;

  if RunAsAdmin
    then sei.lpVerb := 'runas'
    else sei.lpVerb := 'open';

  sei.lpFile := PChar(aCmd);
  sei.lpParameters := PChar(aParams);
  sei.nShow := aShow; // default should be : SW_HIDE

  if not ShellExecuteEx(@sei) then RaiseLastOSError;
  if sei.hProcess <> 0 then
  begin
    while WaitForSingleObject(sei.hProcess, 50) = WAIT_TIMEOUT do Application.ProcessMessages;
    item.ProcessID := sei.hProcess;
    CloseHandle(sei.hProcess);
  end;
end;

procedure TExecuteCommands.ExecuteAndWaitWithOutput(const item: ICommandItem; const aCmd, aParams: string; const RunAsAdmin: Boolean; const aShow: integer);
const
  CReadBuffer = 2400;
var
  saSecurity: TSecurityAttributes;
  hRead: THandle;
  hWrite: THandle;
  suiStartup: TStartupInfo;
  piProcess: TProcessInformation;
  pBuffer: array[0..CReadBuffer] of AnsiChar;
  dRead: DWord;
  dRunning: DWord;
begin
  saSecurity.nLength := SizeOf(TSecurityAttributes);
  saSecurity.bInheritHandle := True;
  saSecurity.lpSecurityDescriptor := nil;

  if CreatePipe(hRead, hWrite, @saSecurity, 0) then
  begin
    FillChar(suiStartup, SizeOf(TStartupInfo), #0);
    suiStartup.cb := SizeOf(TStartupInfo);
    suiStartup.hStdInput := hRead;
    suiStartup.hStdOutput := hWrite;
    suiStartup.hStdError := hWrite;
    suiStartup.dwFlags := STARTF_USESTDHANDLES or STARTF_USESHOWWINDOW;
    suiStartup.wShowWindow := aShow;  // default should be : SW_HIDE

    if CreateProcess(nil, PChar(aCmd + ' ' + aParams), @saSecurity,
          @saSecurity, True, NORMAL_PRIORITY_CLASS, nil, nil, suiStartup, piProcess) then
    begin
     repeat
       dRunning  := WaitForSingleObject(piProcess.hProcess, 100);
       Application.ProcessMessages();
       repeat
         dRead := 0;
         ReadFile(hRead, pBuffer[0], CReadBuffer, dRead, nil);
         pBuffer[dRead] := #0;

         OemToAnsi(pBuffer, pBuffer);

         case item.OutputType of
           otEvented:
              begin
                if Assigned(fOnLine) then fOnLine(fItemIndex,string(pBuffer));
              end;
           otLines:
              begin
                item.Output.Add(string(pBuffer));
              end;
         end;
       until (dRead < CReadBuffer);
     until (dRunning <> WAIT_TIMEOUT);

     CloseHandle(piProcess.hProcess);
     CloseHandle(piProcess.hThread);
    end;

   CloseHandle(hRead);
   CloseHandle(hWrite);
  end;
end;

{ === Event notifications back === }

procedure TExecuteCommands.CommandComplete(const aCommand: ICommandItem);
var
  item: ICommandItem;
begin
  // Update the item with the ProcessID first
  item := fCommands.Items[fItemIndex];
  item.ProcessID := aCommand.ProcessID; // If it's a threaded run this will always be empty or zero
  if Assigned( fOnCommandComplete ) then fOnCommandComplete(fItemIndex);
end;

procedure TExecuteCommands.CommandFailed(const aCommand: ICommandItem; const error: string);
var
  item: ICommandItem;
begin
  // Update the item with the ProcessID first
  item := fCommands.Items[fItemIndex];
  item.ProcessID := aCommand.ProcessID; // If it's a threaded run this will always be empty or zero
  if Assigned( fOnFail ) then fOnFail(fItemIndex, error);
end;


procedure TExecuteCommands.Complete;
begin
  if Assigned( fOnComplete ) then fOnComplete( self );
end;

{ === Accessor methods === }

function TExecuteCommands.GetItems: IList<ICommandItem>;
begin
  Result := fCommands.Items;
end;

function TExecuteCommands.GetItemIndex: integer;
begin
  Result := fItemIndex;
end;

function TExecuteCommands.GetOnCommandComplete: TOnCommandComplete;
begin
  Result := fOnCommandComplete;
end;

procedure TExecuteCommands.SetOnCommandComplete(const Value: TOnCommandComplete);
begin
  fOnCommandComplete := Value;
end;

function TExecuteCommands.GetOnComplete: TNotifyEvent;
begin
  Result := fOnComplete;
end;


procedure TExecuteCommands.SetOnComplete(const Value: TNotifyEvent);
begin
  fOnComplete := Value;
end;

function TExecuteCommands.GetOnFail: TOnFail;
begin
  Result := fOnFail;
end;

procedure TExecuteCommands.SetOnFail(const Value: TOnFail);
begin
  fOnFail := Value;
end;

function TExecuteCommands.GetOnLine: TOnLine;
begin
  Result := fOnLine;
end;

procedure TExecuteCommands.SetOnLine(const Value: TOnLine);
begin
  fOnLine := Value;
end;

function TExecuteCommands.GetSettings: ISettings;
begin
  Result := fSettings;
end;

procedure TExecuteCommands.SetSettings(const Value: ISettings);
begin
  fSettings := Value;
end;

end.
