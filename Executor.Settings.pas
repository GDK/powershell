unit Executor.Settings;

interface

uses
  System.Classes,
  System.SysUtils,
  Executor.Interfaces;

type

  TSettings = class(TInterfacedObject, ISettings)
  private
    fCurrentFolder: string;
    fOutputFolder: string;
    function GetCurrentFolder: string;
    function GetOutputFolder: string;
    procedure SetCurrentFolder(const Value: string);
    procedure SetOutputFolder(const Value: string);
  public
    property OutputFolder : string read GetOutputFolder write SetOutputFolder;
    property CurrentFolder : string read GetCurrentFolder write SetCurrentFolder;
  end;

implementation

{ TSettings }

function TSettings.GetCurrentFolder: string;
begin
  Result := fCurrentFolder;
end;

function TSettings.GetOutputFolder: string;
begin
  Result := fOutputFolder;
end;

procedure TSettings.SetCurrentFolder(const Value: string);
begin
  fCurrentFolder := Value;
end;

procedure TSettings.SetOutputFolder(const Value: string);
begin
  fOutputFolder := Value;
end;

end.

