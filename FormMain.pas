unit FormMain;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ComCtrls,
  Vcl.PlatformDefaultStyleActnCtrls, System.Actions, Vcl.ActnList, Vcl.ActnMan,
  Vcl.ExtCtrls, Vcl.StdCtrls,
  System.IniFiles,

  System.Math,
  System.StrUtils,
  System.RegularExpressions,

  Spring.Collections,

  Executor.Interfaces,
  Executor.Settings,
  Executor.CommandItem,
  Executor.Commands,
  Executor.ExecuteCommands,

  Vcl.CheckLst, dxGDIPlusClasses, System.ImageList,
  Vcl.ImgList;

type

  TMain = class(TForm)
    BottomPanel: TPanel;
    RightPanel: TPanel;
    LeftPanel: TPanel;
    ActionManager: TActionManager;
    PageControl: TPageControl;
    tsAnalyse: TTabSheet;
    tsResults: TTabSheet;
    OpenFolder: TFileOpenDialog;
    edOutputFolder: TEdit;
    lblFolder: TLabel;
    btnSelectOutputFolder: TButton;
    ButtonRun: TButton;
    ButtonLoadConfig: TButton;
    ButtonSaveConfig: TButton;
    OpenFile: TFileOpenDialog;
    edCommandStr: TEdit;
    Label2: TLabel;
    btnAddCommand: TButton;
    TopPanel: TPanel;
    LogoImage: TImage;
    OpenIniFile: TFileOpenDialog;
    SaveIniFile: TFileSaveDialog;
    Label4: TLabel;
    edCurrentFolder: TEdit;
    btnSelectCurrentFolder: TButton;
    ResultsPanel: TPanel;
    memoProgress: TMemo;
    MessagesPanel: TPanel;
    MessageLabel: TLabel;
    btnSelectCommand: TButton;
    ButtonImageList: TImageList;
    ButtonStop: TButton;
    Commands: TMemo;
    cboCommandType: TComboBox;
    cboOutputType: TComboBox;
    chkThreaded: TCheckBox;
    Label1: TLabel;
    Label3: TLabel;
    Label5: TLabel;
    btnClear: TButton;
    chkAutoDelete: TCheckBox;

    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormDestroy(Sender: TObject);

    procedure btnSelectOutputFolderClick(Sender: TObject);
    procedure ButtonLoadConfigClick(Sender: TObject);
    procedure ButtonSaveConfigClick(Sender: TObject);
    procedure edOutputFolderChange(Sender: TObject);
    procedure btnAddCommandClick(Sender: TObject);
    procedure ButtonRunClick(Sender: TObject);
    procedure btnSelectCurrentFolderClick(Sender: TObject);
    procedure CancelButtonClick(Sender: TObject);
    procedure btnClearClick(Sender: TObject);
    procedure btnSelectCommandClick(Sender: TObject);
  private
    { Private declarations }
    fStopped: boolean;
    fTotalFiles: integer;
    fTotalProcessed: integer;
    fCommands: ICommands;
    fSettings: ISettings;
    fExecutor: IExecuteCommands;

    function CreateItem: ICommandItem;
    function GetItem(const aIndex: integer): ICommandItem;

    procedure ProcessFile(const aIndex: integer);
//    procedure ProcessResults;
    procedure WriteToDebugLog(const aValue: string);
    procedure WriteSearchProgressToMemo(const aFilename: string);
//    procedure WriteProgressToMemo(const aFilename: string; const aIndex: integer);

    procedure ExecutorOnFail(const index: integer; const error: string);
    procedure ExecutorOnLine(const index: integer; const line: string);
    procedure ExecutorCommandComplete(const aIndex: integer);
    procedure ExecutorComplete(Sender: TObject);

  public
    { Public declarations }
  end;

var
  Main: TMain;

implementation

uses
  System.Threading,
  Winapi.ShellAPI;

{$R *.dfm}

procedure TMain.FormCreate(Sender: TObject);
begin
  fCommands := TCommands.Create;
  fSettings := TSettings.Create;
  tsResults.Enabled := False;
end;

procedure TMain.FormDestroy(Sender: TObject);
begin
  if Assigned( fExecutor ) then
  begin
    fExecutor.OnLine := nil;
    fExecutor.OnFail := nil;
    fExecutor.OnCommandComplete := nil;
    fExecutor.OnComplete := nil;
  end;
end;

procedure TMain.FormShow(Sender: TObject);
begin
  Self.Caption := 'Executor - []';
end;

procedure TMain.btnSelectOutputFolderClick(Sender: TObject);
begin
  if OpenFolder.Execute then
  begin
    edOutputFolder.Text := OpenFolder.FileName;
    fSettings.OutputFolder := edOutputFolder.Text;
  end;
end;

procedure TMain.btnSelectCurrentFolderClick(Sender: TObject);
begin
  if OpenFolder.Execute then
  begin
    edCurrentFolder.Text := OpenFolder.FileName;
    fSettings.CurrentFolder := edCurrentFolder.Text;
  end;
end;

procedure TMain.edOutputFolderChange(Sender: TObject);
begin
  fSettings.OutputFolder := edOutputFolder.Text;
end;

function TMain.CreateItem: ICommandItem;
begin
  Result := TCommandItem.Create;
  Result.Command := edCommandStr.Text;
  Result.Settings := fSettings;
  Result.Threaded := chkThreaded.Checked;

  // Types are : ctUnknown, ctCMD, ctPowershell, ctPSCore
  case cboCommandType.ItemIndex of
    0 : Result.CommandType := ctUnknown;
    1 : Result.CommandType := ctCMD;
    2 : Result.CommandType := ctPowershell;
    3 : Result.CommandType := ctPSCore;
  end;

  // Types are : otUnknown, otOutFile, otEvented, otLines
  case cboOutputType.ItemIndex of
    0 : Result.OutputType := otUnknown;
    1 : Result.OutputType := otOutFile;
    2 : Result.OutputType := otEvented;
    3 : Result.OutputType := otLines;
  end;

  Result.ProcessID := 0;
end;

procedure TMain.btnClearClick(Sender: TObject);
begin
  fCommands.Clear;
  Commands.Lines.Clear;
end;

procedure TMain.btnAddCommandClick(Sender: TObject);
var
  aItem: ICommandItem;
begin
  aItem := CreateItem;
  fCommands.AddItem(aItem);
  Commands.Lines.Add(aItem.Command);
end;

procedure TMain.btnSelectCommandClick(Sender: TObject);
begin
  // Types are : ctUnknown, ctCMD, ctPowershell, ctPSCore
  case cboCommandType.ItemIndex of
    0, 1 :
      begin
        OpenFile.Title := 'Batch files';
        OpenFile.DefaultExtension := '*.bat';
        OpenFile.FileTypes.Clear;

        with OpenFile.FileTypes.Add do
        begin
          DisplayName := 'Batch files';
          FileMask := '*.bat'
        end;
      end;
    2, 3 :
      begin
        OpenFile.Title := 'Powershell files';
        OpenFile.DefaultExtension := '*.ps1';
        OpenFile.FileTypes.Clear;

        with OpenFile.FileTypes.Add do
        begin
          DisplayName := 'Batch files';
          FileMask := '*.bat'
        end;
      end;
  end;

  with OpenFile.FileTypes.Add do
  begin
    DisplayName := 'All files';
    FileMask := '*.*'
  end;

  if OpenFile.Execute then edCommandStr.Text := OpenFile.FileName;
end;

procedure TMain.ButtonLoadConfigClick(Sender: TObject);
var
  configFile: string;
  iniFile: TIniFile;
begin
  OpenIniFile.Title := 'Load Configuration';
  if OpenIniFile.Execute then
  begin
    configFile := OpenIniFile.FileName;
    Self.Caption := Format('Command Walker - [%s]',[ExtractFileName(configFile)]);
  end;

  if FileExists(configFile) then
  begin
    iniFile := TIniFile.Create(configFile);

    try
      edOutputFolder.Text := iniFile.ReadString('Config','OutputFolder','.');
      edCurrentFolder.Text := iniFile.ReadString('Config','CurrentFolder','.');
      edCommandStr.Text := iniFile.ReadString('Config','Command','');
      chkThreaded.Checked := iniFile.ReadBool('Config','Threaded',False);
      chkAutoDelete.Checked := iniFile.ReadBool('Config','AutoDelete',False);
      cboCommandType.ItemIndex := iniFile.ReadInteger('Config','CommandType',2);
      cboOutputType.ItemIndex := iniFile.ReadInteger('Config','OutputType',1);
    finally
      iniFile.Free;
    end;
  end;
end;

procedure TMain.ButtonSaveConfigClick(Sender: TObject);
var
  configFile: string;
  iniFile: TIniFile;
begin
  SaveIniFile.Title := 'Save Configuration';
  if SaveIniFile.Execute then
  begin
    configFile := SaveIniFile.FileName;
    Self.Caption := Format('Executor - [%s]',[ExtractFileName(configFile)]);
  end;

  iniFile := TIniFile.Create(configFile);
  try
    iniFile.WriteString('Config','OutputFolder',edOutputFolder.Text);
    iniFile.WriteString('Config','CurrentFolder',edCurrentFolder.Text);
    iniFile.WriteString('Config','Command',edCommandStr.Text);
    iniFile.WriteBool('Config','Threaded',chkThreaded.Checked);
    iniFile.WriteBool('Config','AutoDelete',chkAutoDelete.Checked);
    iniFile.WriteInteger('Config','CommandType',cboCommandType.ItemIndex);
    iniFile.WriteInteger('Config','OutputType',cboOutputType.ItemIndex);
  finally
    iniFile.Free;
  end;
end;

procedure TMain.WriteToDebugLog(const aValue: string);
var
  myFile : TextFile;
begin
  AssignFile(myFile, '.\Debug.txt');
  if not FileExists('.\Debug.txt')
    then ReWrite(myFile)
    else Append(myFile);
  WriteLn(myFile,aValue);
  CloseFile(myFile);
end;

{ === Run start === }

procedure TMain.ButtonRunClick(Sender: TObject);
var
  item: ICommandItem;
begin
  MessageLabel.Caption := '';
  fStopped := False;
  fTotalFiles := 0;
  fTotalProcessed := 0;
  tsResults.Enabled := True;

  fExecutor := TExecuteCommands.Create;

  for item in fCommands.Items do fExecutor.AddItem(item);

  //fExecutor.Items := fCommands;

  fExecutor.Settings := fSettings;
  fExecutor.OnLine := ExecutorOnLine;
  fExecutor.OnFail := ExecutorOnFail;
  fExecutor.OnCommandComplete := ExecutorCommandComplete;
  fExecutor.OnComplete := ExecutorComplete;

  PageControl.ActivePage := tsResults;
  MessageLabel.Caption := 'Executing commands ... ';
  memoProgress.Lines.Clear;

  fExecutor.Execute;
end;

procedure TMain.CancelButtonClick(Sender: TObject);
begin
  MessageLabel.Caption := 'Stopping, please wait ... ';
  fExecutor.Stop;
end;

procedure TMain.WriteSearchProgressToMemo(const aFilename: string);
var
  aCount: integer;
begin
  aCount := memoProgress.Lines.Count;
  memoProgress.Lines.BeginUpdate;
  memoProgress.Lines[aCount-1] := Format('  Found : %s ',[aFilename]);
  memoProgress.Lines.EndUpdate;
end;

//procedure TMain.WriteProgressToMemo(const aFilename: string; const aIndex: integer);
//var
//  aCount: integer;
//  aPercent: double;
//begin
//  aCount := memoProgress.Lines.Count;
//  aPercent := RoundTo( ((aIndex/fCommand.Items.Count)*100), -1 );
//  memoProgress.Lines.BeginUpdate;
//  memoProgress.Lines[aCount-2] := Format('  Found     : %s',[aFilename]);
//  memoProgress.Lines[aCount-1] := Format('  Progress  : %d of %d, %s%% complete ',[aIndex,fTotalFiles,aPercent.ToString]);
//  memoProgress.Lines.EndUpdate;
//  ProgressBar.Position := Round(aPercent);
//end;

procedure TMain.ProcessFile(const aIndex: integer);
var
  i: integer;
  outFile: string;
  item: ICommandItem;

  procedure AddLinesFromFile;
  var
    line: string;
    aLines: TStringList;
  begin
    try
      aLines := TStringList.Create;
      aLines.LoadFromFile( IncludeTrailingPathDelimiter(fSettings.OutputFolder)+outFile);

      memoProgress.Lines.Add( format('%s,%s ->',[item.ID.ToString,item.Command]) );
      memoProgress.Lines.Add( '' );
      for line in aLines do memoProgress.Lines.Add( format('%s',[line]) );
      memoProgress.Lines.Add( '' );
    finally
      FreeAndNil(aLines);
    end;
  end;

begin
  item := GetItem(aIndex);
  if (item <> nil) and (not fStopped) then
  begin
    outfile := item.Outfile;
    if item.OutputType = otOutFile then
    begin
      AddLinesFromFile;
      if chkAutoDelete.Checked then DeleteFile(outfile);
    end;

    if item.OutputType = otLines then
    begin
      memoProgress.Lines.Add( format('%s,%s ->',[item.ID.ToString,item.Command]) );
      memoProgress.Lines.Add( '' );
      for i := 0 to item.Output.Count -1 do memoProgress.Lines.Add( format('%s',[item.Output[i]]) );
      memoProgress.Lines.Add( '' );
    end;
  end;
end;

{ === Executor Events coming back === }

procedure TMain.ExecutorCommandComplete(const aIndex: integer);
var
  aItem: ICommandItem;
begin
  aItem := GetItem(aIndex);
  if aItem <> nil then
  begin
    if aItem.OutputType in [otLines,otOutFile] then ProcessFile(aIndex);
  end;
end;

procedure TMain.ExecutorComplete(Sender: TObject);
begin
  if fExecutor.HasStopped
    then MessageLabel.Caption := 'Stopped; The process was halted; no results. '
    else MessageLabel.Caption := 'Execution complete ... ';

  fExecutor.OnCommandComplete := nil;
  fExecutor.OnComplete := nil;
  fExecutor.OnLine := nil;
  fExecutor.OnFail := nil;
end;

procedure TMain.ExecutorOnFail(const index: integer; const error: string);
begin
  MessageLabel.Caption := 'Command failed; stopping, please wait ... ';
  fExecutor.Stop;
end;

procedure TMain.ExecutorOnLine(const index: integer; const line: string);
var
  i: integer;
  item: ICommandItem;
begin
  //item := GetItem(index);
  //if (item <> nil) and (not fStopped) then
  begin
    //if item.OutputType = otEvented then
    begin
      memoProgress.Lines.Add( format('%s',[line]) );
      //memoProgress.Lines.Add( format('%s,%s -> %s',[item.ID.ToString,item.Command,line]) );
    end;
  end;
end;

function TMain.GetItem(const aIndex: integer): ICommandItem;
begin
  Result := fCommands.Items[aIndex];
end;

end.
