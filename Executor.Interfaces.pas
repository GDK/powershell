unit Executor.Interfaces;

interface

uses
  System.Classes,
  Spring.Collections;

type
  TCommandStatus = ( csUnknown, csStarted, csCompleted, csFailed );
  TOutputType = ( otUnknown, otOutFile, otEvented, otLines );
  TCommandType = ( ctUnknown, ctCMD, ctPowershell, ctPSCore );
  TOnFail = procedure(const index: integer; const error: string) of object;
  TOnLine = procedure(const index: integer; const line: string) of object;
  TOnCommandComplete = procedure(const index: integer) of object;

  ISettings = interface
    ['{8BC029F3-C568-4989-A7B5-82BAEF5BE45F}']
    procedure SetOutputFolder(const Value: string);
    function GetOutputFolder: string;
    procedure SetCurrentFolder(const Value: string);
    function GetCurrentFolder: string;

    { Public }
    property OutputFolder : string read GetOutputFolder write SetOutputFolder;
    property CurrentFolder : string read GetCurrentFolder write SetCurrentFolder;
  end;


  ICommandItem = interface
    ['{F8FAC482-6F7F-49D4-9779-D99B14B3005B}']
    function GetID: TGUID;
    function GetProcessID: System.NativeUInt;
    procedure SetProcessID(const Value: System.NativeUInt);
    procedure SetCommand(const Value: string);
    function GetCmd: string;
    function GetParam: string;
    procedure SetCmdAndParam;
    function GetCommand: string;
    function GetOutfile: string;
    procedure SetOutfile(const Value: string);
    function GetCommandType: TCommandType;
    procedure SetCommandType(const Value: TCommandType);
    function GetThreaded: boolean;
    procedure SetThreaded(const Value: boolean);
    procedure SetOutputType(const Value: TOutputType);
    function GetOutputType: TOutputType;
    function GetSettings: ISettings;
    procedure SetSettings(const Value: ISettings);
    function GetOutput: IList<String>;
    function GetStatus: TCommandStatus;

    { Public }
    property ID : TGUID read GetID;
    property ProcessID : System.NativeUInt read GetProcessID write SetProcessID;
    property CommandType : TCommandType read GetCommandType write SetCommandType;
    property Cmd: string read GetCmd;
    property Param: string read GetParam;
    property Command : string read GetCommand write SetCommand;
    property Threaded : boolean read GetThreaded write SetThreaded;
    property OutputType : TOutputType read GetOutputType write SetOutputType;
    property Output : IList<String> read GetOutput;
    property Outfile: string read GetOutfile write SetOutfile;
    property Settings : ISettings read GetSettings write SetSettings;
    property Status : TCommandStatus read GetStatus;
  end;

  ICommands = interface
    ['{E926A467-0F3A-4642-85DB-A9B6D1183BFE}']
    function First: ICommandItem;
    function Next: ICommandItem;
    { Public }
    procedure Clear;
    procedure AddItem(const aItem: ICommandItem);
    function FindItem(const aID: string): ICommandItem;
    function Items: IList<ICommandItem>;
  end;

  IExecuteCommands = interface
    ['{0ED758B4-562B-4D80-A13F-446ED6E231F4}']
    function GetItemIndex: integer;
    function GetSettings: ISettings;
    procedure SetSettings(const Value: ISettings);
    function GetOnCommandComplete: TOnCommandComplete;
    procedure SetOnCommandComplete(const Value: TOnCommandComplete);
    function GetOnLine: TOnLine;
    procedure SetOnLine(const Value: TOnLine);
    function GetOnFail: TOnFail;
    procedure SetOnFail(const Value: TOnFail);
    function GetItems: IList<ICommandItem>;
    function GetOnComplete: TNotifyEvent;
    procedure SetOnComplete(const Value: TNotifyEvent);

    procedure ProcessList;
    procedure ProcessCommand(const aCommand: ICommandItem );

    { Public }
    procedure AddItem(item: ICommandItem);
    procedure Execute;
    procedure Stop;
    function HasStopped: boolean;

    property Items: IList<ICommandItem> read GetItems;
    property ItemIndex: integer read GetItemIndex;
    property Settings: ISettings read GetSettings write SetSettings;
    property OnLine: TOnLine read GetOnLine write SetOnLine;
    property OnFail: TOnFail read GetOnFail write SetOnFail;
    property OnCommandComplete: TOnCommandComplete read GetOnCommandComplete write SetOnCommandComplete;
    property OnComplete: TNotifyEvent read GetOnComplete write SetOnComplete;
  end;

implementation

end.
